package light.notepad;

import java.util.ArrayList;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;


public class NotepadActivity extends ListActivity {
	

	//---------------------------------------------------------->main
	public static final int CHECK_STATE = 0;
	public static final int EDIT_STATE = 1;
	public static final int ALERT_STATE = 2;
	
	
	
	// View addView;
	 private Button addButton;
	 private ListView listView;
	 private ListViewAdapeter adapter;
	 
	private DatabaseManager dm = null;// 数据库管理对象
	 private Cursor cursor = null;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
     //   addView = getLayoutInflater().inflate(R.layout.add_button, null); //不在同个xml中 需要先找到整个layout
        addButton = (Button)findViewById(R.id.addButton);
        addButton.setOnClickListener(new AddButtonListener());
        
       dm = new DatabaseManager(this); 
       
      
       initListView(); 
       setListAdapter(adapter);
       listView.setOnCreateContextMenuListener(new MenuCreateListener());
       
       // listView.setOnScrollListener(this);
    }
	
	
    // ------------------------------------------------------>ListView
	public void initListView(){
	//	listView = (ListView)findViewById(R.id.muListView); 
		listView = getListView();
		dm.open();
		
		cursor = dm.selectAll();
		cursor.moveToFirst();
		int count = cursor.getCount();
		
		ArrayList<String> items = new ArrayList<String>();
		ArrayList<String> time = new ArrayList<String>();
		
		for(int i=0;i<count;i++){
			items.add(cursor.getString(cursor.getColumnIndex("title")));
			time.add(cursor.getString(cursor.getColumnIndex("time")));
			cursor.moveToNext();
		}
		dm.close();
		
		adapter = new ListViewAdapeter(this, items, time);
		//listView.setAdapter(new ListViewAdapeter(this, items, time));
		
	}
	
	
	//点击list时
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		cursor.moveToPosition(position);
		Intent intent = new Intent();
		
		intent.putExtra("state", ALERT_STATE);
		intent.putExtra("id", cursor.getString(cursor.getColumnIndex("_id")));
		intent.putExtra("title", cursor.getString(cursor.getColumnIndex("title")));
		intent.putExtra("time", cursor.getString(cursor.getColumnIndex("time")));
		intent.putExtra("content", cursor.getString(cursor.getColumnIndex("content")));
		
		dm.close();
		
		intent.setClass(NotepadActivity.this, CheckListActivity.class);
		NotepadActivity.this.startActivity(intent);
	}


//长按菜单
	public class MenuCreateListener implements OnCreateContextMenuListener{

	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		final AdapterView.AdapterContextMenuInfo info = 
				(AdapterView.AdapterContextMenuInfo) menuInfo;
		menu.setHeaderTitle("");
		menu.add(0, 0, 0, "删除");
		menu.add(0, 1, 0, "修改");
		}
	}
	
//长按后的事件
	 @Override
		public boolean onContextItemSelected(MenuItem item) {
		 AdapterView.AdapterContextMenuInfo menuInfo = 
					(AdapterView.AdapterContextMenuInfo)item
					.getMenuInfo();
		 dm.open();
		 switch(item.getItemId()){
		 
		 case 0:
		 try{
			 cursor.moveToPosition(menuInfo.position);
			 int i = dm.delete(Long.parseLong(cursor.getString(cursor.getColumnIndex("_id"))));
			 
			 adapter.removeItem(menuInfo.position);
			 adapter.notifyDataSetChanged();//通知数据源，数据已经改变，刷新界面
			 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 break;
		 //修改
		 case 1:    
			 try{
				 cursor.moveToPosition(menuInfo.position);
				 Intent intent = new Intent();
				 
				 intent.putExtra("state", ALERT_STATE);
				 intent.putExtra("id", cursor.getString(cursor.getColumnIndex("_id")));
				 intent.putExtra("title", cursor.getString(cursor.getColumnIndex("title")));
				 intent.putExtra("time", cursor.getString(cursor.getColumnIndex("time")));
				 intent.putExtra("content", cursor.getString(cursor.getColumnIndex("content")));

				 intent.setClass(NotepadActivity.this, EditTextActivity.class);
				 NotepadActivity.this.startActivity(intent);
			 }catch(Exception e){
				 e.printStackTrace();
			 }
		 
		 break;
			default:;
		 }
		 dm.close();
		 return super.onContextItemSelected(item);
		}
	

	
	public class AddButtonListener implements OnClickListener{

		public void onClick(View v) {
			Intent intent = new Intent();
			intent.putExtra("state", EDIT_STATE);
			intent.setClass(NotepadActivity.this, EditTextActivity.class);
			NotepadActivity.this.startActivity(intent);
		}
	}





	




	




	
	
}